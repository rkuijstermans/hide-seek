using System;
using System.Collections.Generic;
using System.Linq;

namespace HadeAndSeek
{
    public class Game : IGame
    {
        private readonly IView view;
        private readonly IEnumerable<IPlayerInputBuffer> inputBuffers;
        private readonly IEnumerable<Player> players;
        private readonly IMap map;

        public Game(IClientFactory factory, IMap map)
        {
            if (factory == null)
            {
                throw new ArgumentNullException(nameof(factory));
            }

            if (map == null)
            {
                throw new ArgumentNullException(nameof(map));
            }

            this.map = map;

            Player player1 = new Player("Player 1", GeneratePlayerLocation());
            Player player2 = new Player("Player 2", GeneratePlayerLocation());

            players = new Player[] { player1, player2 };

            IGameController player1Controller = factory.CreateBasicController(player1, KeyLayout.Wasd);
            IGameController player2Controller = factory.CreateBasicController(player2, KeyLayout.Arrows);

            inputBuffers = new List<IPlayerInputBuffer>()
            {
                new PlayerInputBuffer(player1Controller),
                new PlayerInputBuffer(player2Controller)
            };

            this.view = factory.CreateView();
        }

        private Coords GeneratePlayerLocation()
        {
            Random rnd = new Random();

            Tile tile = map.GetAllTiles()
                .Where(t => t.Walkable)
                .OrderBy(t => rnd.Next())
                .First();
            
            return tile.Coordinates;
        }

        public IEnumerable<IPlayerInputBuffer> InputBuffers => inputBuffers;

        public IEnumerable<Player> Players => players;

        public IView View => view;

        public IMap Map => map;
    }
}
