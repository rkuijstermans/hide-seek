using System;

namespace HadeAndSeek
{
    public static class PlayerInputExtensions
    {
        public static MapDirection ToMapDirection(this PlayerInput input)
        {
            switch (input)
            {
                case PlayerInput.MoveDown:
                    return MapDirection.Down;
                case PlayerInput.MoveLeft:
                    return MapDirection.Left;
                case PlayerInput.MoveRight:
                    return MapDirection.Right;
                case PlayerInput.MoveUp:
                    return MapDirection.Up;
                default:
                    throw new ArgumentException("input cannot be converted to MapDirection");
            }
        }
    }
}