using System;

namespace HadeAndSeek
{
    public class PlayerInputEventArgs : EventArgs
    {
        public PlayerInputEventArgs(PlayerInput input)
        {
            Input = input;
        }

        public PlayerInput Input
        { get; private set; }
    }
}
