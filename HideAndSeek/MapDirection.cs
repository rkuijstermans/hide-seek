using System;

namespace HadeAndSeek
{
    [Flags]
    public enum MapDirection
    {
        Up = 1,
        Down = 2,
        Left = 4,
        Right = 8
    }

    public static class MapDirectionExtensions
    {
        public static bool Contains(this MapDirection value, MapDirection containing)
        {
            return (value & containing) == containing;
        }
    }
}
