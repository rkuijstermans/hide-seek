namespace HadeAndSeek
{
    public enum PlayerInput
    {
        MoveUp,
        MoveDown,
        MoveLeft,
        MoveRight,
        Exit
    }
}