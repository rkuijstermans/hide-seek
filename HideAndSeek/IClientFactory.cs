namespace HadeAndSeek
{
    public interface IClientFactory
    {
        IGameController CreateBasicController(Player player, KeyLayout layout);

        IView CreateView();
    }
}