using System;

namespace HadeAndSeek
{
    public struct Coords : IComparable<Coords>, IEquatable<Coords>
    {
        public Coords(int x, int y)
        {
            X = x;
            Y = y;
        }

        public int X
        { get; private set; }

        public int Y
        { get; private set; }

        public bool IsInRange(Coords other, int range)
        {
            int diff = Math.Abs(X - other.X) + Math.Abs(Y - other.Y);

            return diff <= range;
        }

        public int CompareTo(Coords other)
        {
            int xCompare = X.CompareTo(other.X);
            int yCompare = 0;

            if (xCompare == 0)
            {
                yCompare = Y.CompareTo(other.Y);
            }

            return xCompare + yCompare;
        }

        public override bool Equals(object other)
        {
            if (other is Coords)
            {
                return this.Equals((Coords)other);
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return Tuple.Create(X, Y).GetHashCode();
        }

        public bool Equals(Coords other)
        {
            return X == other.X && Y == other.Y;
        }

        public static bool operator <(Coords left, Coords right)
        {
            return left.CompareTo(right) < 0;
        }

        public static bool operator >(Coords left, Coords right)
        {
            return left.CompareTo(right) > 0;
        }

        public static bool operator ==(Coords left, Coords right)
        {
            return left.CompareTo(right) == 0;
        }

        public static bool operator !=(Coords left, Coords right)
        {
            return left.CompareTo(right) != 0;
        }

        public static Coords operator +(Coords coords, MapDirection direction)
        {
            int yDelta = 0;
            int xDelta = 0;

            if (direction.Contains(MapDirection.Up))
            {
                yDelta--;
            }

            if (direction.Contains(MapDirection.Down))
            {
                yDelta++;
            }

            if (direction.Contains(MapDirection.Left))
            {
                xDelta--;
            }

            if (direction.Contains(MapDirection.Right))
            {
                xDelta++;
            }

            return new Coords(coords.X + xDelta, coords.Y + yDelta);
        }
    }
}
