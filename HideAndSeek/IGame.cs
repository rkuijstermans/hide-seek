using System.Collections.Generic;

namespace HadeAndSeek
{
    public interface IGame
    {
        IEnumerable<IPlayerInputBuffer> InputBuffers
        { get; }

        IEnumerable<Player> Players
        { get; }

        IMap Map
        { get; }

        IView View
        { get; }
    }
}
