using System;

namespace HadeAndSeek
{
    public interface IGameController
    {
        Player Player
        { get; }

        event EventHandler<PlayerInputEventArgs> KeyInput;
    }
}
