using System;

namespace HadeAndSeek
{
    public class Player
    {
        private readonly string name;

        public Player(string name, Coords location)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentException("name cannot be empty or contain only whitespaces");
            }

            this.name = name;
            this.Location = location;
        }

        public string Name
        { get { return name; }}

        public void Move(IMap map, MapDirection direction)
        {
            if (map == null)
            {
                throw new ArgumentNullException(nameof(map));
            }

            Tile? newTile = map.GetTile(Location + direction);

            if (newTile.HasValue && newTile.Value.Walkable)
            {
                Location = newTile.Value.Coordinates;
            }
        }

        public Coords Location
        { get; private set; }
    }
}
