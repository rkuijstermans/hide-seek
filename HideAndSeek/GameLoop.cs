using System;
using System.Linq;
using System.Threading;

namespace HadeAndSeek
{
    public class GameLoop
    {
        private readonly IGame game;

        public GameLoop(IGame game)
        {
           this.game = game;
        }

        public void Start()
        {
            bool keepRunning;

            do
            {
                keepRunning = PerformLoop();
            }
            while (keepRunning);
        }

        private bool PerformLoop()
        {
            // check user input
            var inputs = game.InputBuffers
                .SelectMany(b => b.RetrieveInput()
                    .Select(i => new { Player = b.Player, Input = i }));
                
            foreach (var i in inputs)
            {
                switch (i.Input)
                {
                    case PlayerInput.Exit:
                        return false; // the gameloop will end
                    case PlayerInput.MoveDown:
                    case PlayerInput.MoveLeft:
                    case PlayerInput.MoveRight:
                    case PlayerInput.MoveUp:
                        i.Player.Move(game.Map, i.Input.ToMapDirection());
                        break;
                    default:
                        throw new InvalidOperationException("Unknown input");
                }
            }

            // Draw the map
            game.View.Draw(game.Map, game.Players);

            // check end conditions
            bool end = game.Players
                .Any(p1 => game.Players
                    .Where(p2 => p2 != p1)
                    .Any(p2 => p1.Location.IsInRange(p2.Location, 1)));

            if (end)
            {
                game.View.DrawEnd();
                return false;
            }

            Thread.Sleep(100);

            return true;
        }
    }
}
