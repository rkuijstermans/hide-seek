using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace HadeAndSeek
{
    internal class PlayerInputBuffer : IPlayerInputBuffer
    {
        private readonly Player player;
        private readonly ConcurrentQueue<PlayerInput> inputs = new ConcurrentQueue<PlayerInput>();

        public PlayerInputBuffer(IGameController controller)
        {
            if (controller == null)
            {
                throw new ArgumentNullException(nameof(controller));
            }

            player = controller.Player;

            controller.KeyInput += (s, e) =>
            {
                inputs.Enqueue(e.Input);
            };
        }

        public Player Player => player;

        public IEnumerable<PlayerInput> RetrieveInput()
        {
            PlayerInput input;

            while (inputs.TryDequeue(out input))
            {
                yield return input;
            }
        }
    }
}