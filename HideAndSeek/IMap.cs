using System.Collections.Generic;

namespace HadeAndSeek
{
    public interface IMap
    {
        Tile? GetTile(Coords coordinates);

        IEnumerable<Tile> GetAllTiles();

        int Width
        { get; }

        int Height
        { get; }
    }
}