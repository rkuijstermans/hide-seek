namespace HadeAndSeek
{
    public struct Tile
    {
        public Tile(int coordX, int coordY, bool walkable)
            :this (new Coords(coordX, coordY), walkable)
        { }

        public Tile(Coords coordinates, bool walkable)
        {
            this.Coordinates = coordinates;
            this.Walkable = walkable;
        }

        public Coords Coordinates
        { get; private set; }

        public bool Walkable
        { get; private set; }
    }
}
