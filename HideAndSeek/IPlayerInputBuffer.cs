using System.Collections.Generic;

namespace HadeAndSeek
{
    public interface IPlayerInputBuffer
    {
        Player Player
        { get; }

        IEnumerable<PlayerInput> RetrieveInput();
    }
}