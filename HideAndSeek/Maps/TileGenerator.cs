using System;
using System.Collections.Generic;
using System.Linq;

namespace HadeAndSeek
{
    public static class TileGenerator
    {
        public static IEnumerable<Tile> CreateFromString(string tileFormat)
        {
            if (tileFormat == null)
            {
                throw new ArgumentNullException(nameof(tileFormat));
            }

            IEnumerable<string> rows = tileFormat.Split(new string[] { Environment.NewLine, "\n" }, StringSplitOptions.None);
            Console.Write(rows.Count());

            return rows
                .SelectMany((row, y) => row
                    .Select((c, x) => new Tile(x, y, c == ' ')));
        }
    }
}