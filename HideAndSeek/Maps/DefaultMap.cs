﻿namespace HideAndSeek.Maps
{
    public static class DefaultMap
    {
        public static string GetFormat()
        {
            return @"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
X                               X         X                    X
X XXXXXXXX XXXXXXXXX XXXXXXXXXX X         X                    X
X X      X X       X XXXXXXXXXX X         X                    X
X X XXXX X X       X XXXXXXXXXX X         XXXXXXXXXXXXXXXXXXXX X
X X X XX X X       XXXXXXXXXXXX X                              X
X X X XX XXXXXX XXXX            XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX X
X   X XX                                                       X
XXXXX XX XXXXXXXXXXX            XXXXX XXXXXXXXXXXXXXXXXXXXX XX X
X           XXXXXXXXXXXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXXXXXXX XX X
X  X     X  X   X                                         X XX X
X           X X X XXXXXXXXX XXXXXXXXX XXXXXXXXX XXXXXXXXX X XX X
X  X     X  X X                                           X XX X
X           X X XXXXXXXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXXXXXXXXXX X
X  X     X  X X X X X X X X XXXXXXXXX X   XXX       X        X X
X           X X             XXXXXXXXX X X     XXXX     XXXXX X X
X  X     X  X XXXXXXXXXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXXXXXXXX X X
X                                     X                        X
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
        }
    }
}
