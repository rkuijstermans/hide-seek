using System;
using System.Collections.Generic;
using System.Linq;

namespace HadeAndSeek.Maps
{
    public class SimpleMap : IMap
    {
        private readonly IEnumerable<Tile> tiles;

        public SimpleMap(IEnumerable<Tile> tiles)
        {
            if (tiles == null)
            {
                throw new ArgumentNullException(nameof(tiles));
            }

            this.tiles = tiles;
        }

        public int Width => tiles.Max(t => t.Coordinates.X);

        public int Height => tiles.Max(t => t.Coordinates.Y);

        public IEnumerable<Tile> GetAllTiles()
        {
            return tiles;
        }

        public Tile? GetTile(Coords coordinates)
        {
            return tiles
                .Where(t => t.Coordinates == coordinates)
                .Select(t => (Tile?)t)
                .SingleOrDefault();
        }
    }
}