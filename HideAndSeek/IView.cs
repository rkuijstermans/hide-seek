using System.Collections.Generic;

namespace HadeAndSeek
{
    public interface IView
    {
        void Draw(IMap map, IEnumerable<Player> players);

        void DrawEnd();
    }
}
