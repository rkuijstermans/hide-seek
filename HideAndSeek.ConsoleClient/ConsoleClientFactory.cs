using System.Threading;

namespace HadeAndSeek.ConsoleClient
{
    public class ConsoleClientFactory : IClientFactory
    {
        private readonly ConsoleKeyListener keyListener = new ConsoleKeyListener();

        public ConsoleClientFactory()
        {
            Thread thread = new Thread(keyListener.StartListening);
            thread.Start();
        }

        public IGameController CreateBasicController(Player player, KeyLayout layout)
        {
            ConsoleGameController controller = new ConsoleGameController(player, ConsoleKeyLayout.GetMapping(layout));
            keyListener.AddController(controller);

            return controller;
        }

        public IView CreateView()
        {
            return new ConsoleView();
        }
    }
}