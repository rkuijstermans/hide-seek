﻿using HadeAndSeek.Maps;
using HideAndSeek.Maps;
using System.Collections.Generic;

namespace HadeAndSeek.ConsoleClient
{
    class Program
    {
        static void Main(string[] args)
        {
            IEnumerable<Tile> tiles = TileGenerator.CreateFromString(DefaultMap.GetFormat());
            IMap map = new SimpleMap(tiles);

            IClientFactory factory = new ConsoleClientFactory();
            IGame game = new Game(factory, map);

            new GameLoop(game).Start();
        }
    }
}
