using System;
using System.Collections.Generic;

namespace HadeAndSeek.ConsoleClient
{
    public class ConsoleGameController : IGameController
    {
        private readonly Player player;
        private  readonly IReadOnlyDictionary<ConsoleKey, PlayerInput> keyMapping;

        public ConsoleGameController(Player player, IReadOnlyDictionary<ConsoleKey, PlayerInput> keyMapping)
        {
            if (player == null)
            {
                throw new ArgumentNullException(nameof(player));
            }

            if (keyMapping == null)
            {
                throw new ArgumentNullException(nameof(keyMapping));
            }

            this.player = player;
            this.keyMapping = keyMapping;
        }

        public Player Player => player;

        public event EventHandler<PlayerInputEventArgs> KeyInput;

        internal void HandleKey(ConsoleKey key)
        {
            PlayerInput input;

            if (keyMapping.TryGetValue(key, out input))
            {
                KeyInput?.Invoke(this, new PlayerInputEventArgs(input));
            }
        }
    }
}