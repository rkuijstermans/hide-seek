using System;
using System.Collections.Generic;

namespace HadeAndSeek.ConsoleClient
{
    internal class ConsoleKeyListener
    {
        private readonly IList<ConsoleGameController> controllers = new List<ConsoleGameController>();

        internal void StartListening()
        {
            while (true)
            {
                ConsoleKey key = Console.ReadKey().Key;

                foreach (ConsoleGameController controller in controllers)
                {
                    controller.HandleKey(key);
                }
            }
        }

        internal void AddController(ConsoleGameController controller)
        {
            controllers.Add(controller);
        }
    }
}