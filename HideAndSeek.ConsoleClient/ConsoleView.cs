using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HadeAndSeek.ConsoleClient
{
    public class ConsoleView : IView
    {
        private const char fog = 'X';
        private const char player = 'o';
        private const char wall = 'X';
        private const char empty = ' ';

        public ConsoleView()
        {
            Console.Clear();
            Console.CursorVisible = false;
        }

        public void Draw(IMap map, IEnumerable<Player> players)
        {
            if (map == null)
            {
                throw new ArgumentNullException(nameof(map));
            }

            if (players == null)
            {
                throw  new ArgumentNullException(nameof(players));
            }

            Console.SetWindowSize(map.Width + 1, map.Height + 2);

            StringBuilder sb = new StringBuilder();

            var rows = map.GetAllTiles()
                .GroupBy(t => t.Coordinates.Y);
                
            foreach (var row in rows)
            {
                Console.SetCursorPosition(0, row.Key);

                string rowString = new string(row
                    .OrderBy(t => t.Coordinates.X)
                    .Select(t => GetTileChar(t, players))
                    .ToArray());
                Console.WriteLine(rowString);
            }

            // draw the players on the map
            foreach (Player p in players)
            {
                Console.SetCursorPosition(p.Location.X, p.Location.Y);
                Console.ForegroundColor = ConsoleColor.Cyan;

                Console.Write(player);
            }

            // Reset the color and position
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.SetCursorPosition(0, 0);
        }

        public void DrawEnd()
        {
            Console.Clear();
            Console.WriteLine("Congratulations! You found each other!");
        }

        private char GetTileChar(Tile tile, IEnumerable<Player> players)
        {
            bool isInRange = players.Any(p => p.Location.IsInRange(tile.Coordinates, 3));

            if (!isInRange)
            {
                return fog;
            }
            else if (!tile.Walkable)
            {
                return wall;
            }
            else
            {
                return empty;
            }
        }
    }
}
