using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace HadeAndSeek.ConsoleClient
{
    public static class ConsoleKeyLayout
    {
        private readonly static IReadOnlyDictionary<ConsoleKey, PlayerInput> arrowMapping;
        private readonly static IReadOnlyDictionary<ConsoleKey, PlayerInput> wasdMapping;

        static ConsoleKeyLayout()
        {
            Dictionary<ConsoleKey, PlayerInput> arrowMappingTmp = new Dictionary<ConsoleKey, PlayerInput>()
            {
                { ConsoleKey.Escape, PlayerInput.Exit },
                { ConsoleKey.UpArrow, PlayerInput.MoveUp },
                { ConsoleKey.DownArrow, PlayerInput.MoveDown },
                { ConsoleKey.LeftArrow, PlayerInput.MoveLeft },
                { ConsoleKey.RightArrow, PlayerInput.MoveRight },
            };

            arrowMapping = new ReadOnlyDictionary<ConsoleKey, PlayerInput>(arrowMappingTmp);
            
            Dictionary<ConsoleKey, PlayerInput> wasdMappingTmp = new Dictionary<ConsoleKey, PlayerInput>()
            {
                { ConsoleKey.Escape, PlayerInput.Exit },
                { ConsoleKey.W, PlayerInput.MoveUp },
                { ConsoleKey.S, PlayerInput.MoveDown },
                { ConsoleKey.A, PlayerInput.MoveLeft },
                { ConsoleKey.D, PlayerInput.MoveRight },
            };

            wasdMapping = new ReadOnlyDictionary<ConsoleKey, PlayerInput>(wasdMappingTmp);
        }

        public static IReadOnlyDictionary<ConsoleKey, PlayerInput> GetMapping(KeyLayout layout)
        {
            switch (layout)
            {
                case KeyLayout.Arrows:
                    return arrowMapping;
                case KeyLayout.Wasd:
                    return wasdMapping;
                default:
                    throw new ArgumentException("Unknown layout");
            }
        }
    }
}